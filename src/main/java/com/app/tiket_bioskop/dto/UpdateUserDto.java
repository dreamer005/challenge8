package com.app.tiket_bioskop.dto;

import lombok.Getter;

@Getter
public class UpdateUserDto {

    private Integer userId;
    private String username;
    private String email;
    private String password;

    public UpdateUserDto() {}

    public UpdateUserDto(String username, String email, String password, Integer userId) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.userId = userId;
    }
}
