package com.app.tiket_bioskop.dto.request;

import com.app.tiket_bioskop.dto.AddFilmDto;
import com.app.tiket_bioskop.dto.UpdateFilmDto;
import com.app.tiket_bioskop.entity.Films;
import org.springframework.stereotype.Component;

@Component
public class RequestFilm {
    public Films Add(AddFilmDto addFilmDto) {
        Films films = new Films();
        films.setFilmCode(addFilmDto.getFilmCode());
        films.setFilmName(addFilmDto.getFilmName());
        films.setSedangTayang(addFilmDto.getSedangTayang());
        return films;
    }

    public Films Update(UpdateFilmDto updateFilmDto) {
        Films films = new Films();
        films.setFilmId(updateFilmDto.getFilmId());
        films.setFilmCode(updateFilmDto.getFilmCode());
        films.setFilmName(updateFilmDto.getFilmName());
        films.setSedangTayang(updateFilmDto.getSedangTayang());
        return films;
    }
}
