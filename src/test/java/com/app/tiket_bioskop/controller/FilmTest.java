package com.app.tiket_bioskop.controller;

import com.app.tiket_bioskop.controller.FilmController;
import com.app.tiket_bioskop.dto.AddFilmDto;
import com.app.tiket_bioskop.dto.UpdateFilmDto;
import com.app.tiket_bioskop.entity.Schedules;
import com.app.tiket_bioskop.repository.FilmRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

@SpringBootTest
class FilmTest {

    @Autowired
    private FilmController filmController;

    @Autowired
    private FilmRepository filmRepository;

    @Test
    @DisplayName("1. Menambahkan film")
    void addFilm() {
        AddFilmDto addFilmDto = new AddFilmDto("KodeNo1", "NamaNo1", 1);
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.CREATED), filmController.addFilm(addFilmDto));
    }

    @Test
    @DisplayName("2. Mengupdate Film")
    void updateUser() {
        UpdateFilmDto updatedFilm = new UpdateFilmDto(1,"UpdateKode2", "UpdateNama2", 0);
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.OK), filmController.updateFilm(updatedFilm));
    }

    @Test
    @DisplayName("3. Menghapus Film")
    void deleteUser() {
        int filmDeleted = 1;
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.ACCEPTED), filmController.deleteFilm(filmDeleted));
    }

    @Test
    @DisplayName("4. Menampilkan Film-film yang sedang tayang")
    void showSedangTayang() {
        Assertions.assertDoesNotThrow(() -> filmController.showAllSedangTayang());
    }

    @Test
    @DisplayName("5. Menampilkan Jadwal suatu film (by id)")
    void showFilmSchedule() {
        int filmId = 4;
        List<Schedules> schedule = filmRepository.showFilmSchedule(filmId);
        filmController.showScheduleFilm(11);
        Assertions.assertEquals(new ResponseEntity<>(schedule, HttpStatus.OK), filmController.showScheduleFilm(filmId));

    }

}
